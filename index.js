// without additional parameters

const http = require('http');
const fs = require('fs');
// var js = require("jsearch");

const hostname = '127.0.0.1';
const port = 3000;
// const axios = require("axios");
var st = require('./lib/stocktwits');

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');

    const filteringData = function (result) {
        console.log('response handed over to promise, started processing');
        fs.writeFile('data.json', JSON.stringify(result), function (err) {
            if (err) {
                return console.log(err);
            }

            console.log('The file was saved, default list!');
        });

        var messagesJson = result.messages;
        if (messagesJson) {
            var messagesFilteredList = [];
            for (var i = 0; i < messagesJson.length; i++) {
                if (messagesJson[i] && messagesJson[i].body && messagesJson[i].body.length > 8) {
                    var messageStr = messagesJson[i].body.toLowerCase().substring(0, 25);
                    // console.log("messageStr");
                    // console.log(messageStr);{
                    if (messageStr.indexOf('🚨') > -1 && (messageStr.indexOf('buy alert') > -1 || messageStr.indexOf('add alert') > -1)) {
                        var currentStockTickerStartIndex = messageStr.indexOf('$');
                        if (currentStockTickerStartIndex > -1) {
                            var messageStrStartAtDollar = messageStr.substring(currentStockTickerStartIndex + 1, messageStr.length);
                            var tickerSymbol = messageStrStartAtDollar.split(' ')[0]; // start at currentStockTickerStartIndex until space hits
                            if (tickerSymbol.length <= 5 && tickerSymbol.length > 0) {
                                messagesFilteredList.push({ ticker: tickerSymbol, comment: messageStr });
                            }
                        }
                    }
                }
            }
            console.log(messagesFilteredList);
            fs.writeFile(
                './filteredData/dataFiltered' + new Date().getTime() + '.json',
                JSON.stringify({ filteredMessages: messagesFilteredList, dateSaved: new Date().getTime(), dateSavedStr: new Date().toISOString() }),
                function (err) {
                    if (err) {
                        return console.log(err);
                    }

                    console.log('The file was saved!');
                }
            );
        }
    };

    const createFetchRequest = function () {
        st.get('streams/user/mrinvestorpro', function (err, res) {
            filteringData(res.body);
        });
    };

    console.log('response start');
    setInterval(function () {
        console.log('start the fetch request');
        createFetchRequest();
    }, 20000); // 20 seoncds with 200 per hour. or 10 seoncds with 400 per hour max cap per hour if authentication.
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
